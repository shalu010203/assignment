import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
   
  url='./../../../assets/';

    sample:any = [
    {
      Logo:'Vistara.png',
      flightName:'Vistara',
      From_location: 'DEL New Delhi, India',
      departure_time: '17:45',
      To_location: 'BOM Mumbai, India',
      arrival_time: '20:50',
      duration:'2h 20m',
      Price: '6000',
      Emi:'2085'
    },
    {
      Logo:'spicejet.png',
      flightName:'Spicejet',
      From_location: 'LUH Ludhiana, India',
      departure_time: '17:45',
      To_location: 'SHL	Shillong, India',
      arrival_time: '20:50',
      duration:'5h 20m',
      Price: '7000',
      Emi:'2085'
    },
    {
      Logo:'airindia.png',
      flightName:'AirIndia',
      From_location: 'BLR Bangalore, India',
      departure_time: '17:45',
      To_location: 'BOM Mumbai, India',
      arrival_time: '20:50',
      duration:'3h 20m',
      Price: '5000',
      Emi:'2085'
    },
    {
      Logo:'indigo.png',
      flightName:'Indigo',
      From_location: 'GOI Goa, India',
      departure_time: '17:45',
      To_location: 'NMB Daman, India',
      arrival_time: '20:50',
      duration:'8h 20m',
      Price: '9000',
      Emi:'2085'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

  filterByDepatureTime()
  {
    var order = false;
    order = !order;
      this.sample.sort(function(a,b){
        return ((a.From_location == b.From_location) ? 0 : ((a.From_location > b.From_location) ? 1 : -1 ))
        }
    );

   
  }


  filterDuration()
  {
      this.sample.sort(function(a,b){
         return ((a.duration == b.duration) ? 0 : ((a.duration > b.duration) ? 1 : -1 ))
        }
    );
  
  }

  filterArrival()
  {
      this.sample.sort(function(a,b){
        return ((a.To_location == b.To_location) ? 0 : ((a.To_location > b.To_location) ? 1 : -1 ))
        }
    );
  
  }

  filterPrice()
  {
      this.sample.sort(function(a,b){
        return a.Price - b.Price;
        }
    );
  
  }
}
